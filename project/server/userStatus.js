/**
 * Created by vincilbishop on 3/17/15.
 */

Meteor.users.find({ "status.online": true }).observe({
  added: function(user) {
    // id just came online
    //console.log('User ' + user._id + ' came online.');

    /*
    var player = {};
    player.userId = user._id;
    var id = Player.insert(player);

    console.log('inserted: ' + id);
    */
  },
  removed: function(user) {
    // id just went offline
    //console.log('User ' + user._id + ' went offline.');

    //Player.remove({userId:user._id});
  }
});